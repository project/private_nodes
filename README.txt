AUTHOR:
-------------
Ben Scott
ben@benscott.co.uk

WHAT IT DOES:
-------------
This module allows users to choose whether a node is private, public or
visible only to friends (if buddylist is installed).
Administrators can set permission to determine which node types offer 
users this option.

TO INSTALL:
-----------
Drop the private_nodes.module into the 'modules' directory
of your drupal installation.

TO ENABLE:
----------
Enable the module as you would any other module.  

TO DISABLE:
-----------
Disable as per any other module.

OTHER NOTES:
------------
This uses the drupal permissioning system to determine which nodes
users can control access to, but node access is not controlled using
Drupal's nodeaccess mechanism (so this works on a per friend basis). Instead it uses hook_menu to check access, and then forces a
403 if access is not allowed. It also uses db_rewrite to remove hidden nodes.

 
